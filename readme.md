# MX100

This API is build to help company MX100 to monitoring and maintain company jobs and freelance that work for MX100

## Getting Started

These instructions will get you how to run this project on your system

### Prerequisites

this project is build to :
1. create and update job (as draft or as publish)
2. get all freelance proposal applied to each job
3. made freelance can apply to job but only one job
4. made freelance to see all job avalilable
```

1. You just need to clone this git link to your local then you can change the connection to database you desire
2. Create Table in your database like database sheme attache on this project
3. Create your own freelance data
4. You are ready to go
```
## Running the tests

You need to test the project is working by using postman or similar software
Before you have to request token for authentication :
1. Send request token to /authenticate using postman with this body
```
{
	"username" : "mx100_dev",
	"password" : "password"
}

You will get response like this

{
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqYXZhaW51c2UiLCJleHAiOjE1NzIzOTAyMzAsImlhdCI6MTU3MjM3MjIzMH0.d1vWFl3U4rMLkq3njYYfgsUeKAVGsm3Xavb_iqumVUkb_ls0a3yWbE-Lx48truuDMYG0kis5OiV9l438EPVGYw"
}
```
Before using any api routes you have to do this
Copy those token value and add it to :
1. Choose Authorization tab on post man
2. Choose Type : Bearer Token
3. Paste the token on token field
```
### Sending code

You need to look at the model structure to make sure that your data is valid
```
{
	"id" : 0,
	"name" : "JS DEVELOPER",
	"published" : 1,
	"createddate" : "",
	"updateddate" : ""
}

There is a bit explanation about logic on each code
```
Response

```

this is one example response when your code is runing well

{
    "message": "Success Publish Job"
}
```

### Project Dependencies

This Project is using  dependencies
1. JPA
2. JDBC
3. MYSQL
4. jsonwebtoken
5. spring-boot-starter-security
```

## Authors

* **Muhammad Tsany Dzulkiflfi

