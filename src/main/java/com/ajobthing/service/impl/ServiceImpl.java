package com.ajobthing.service.impl;

import java.sql.Connection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajobthing.connection.ConnectionDB;
import com.ajobthing.dao.Dao;
import com.ajobthing.model.Job;
import com.ajobthing.model.Proposal;

@Service
public class ServiceImpl extends ConnectionDB implements com.ajobthing.service.Service{
	
	@Autowired
	Dao dao;

	@Override
	public String insertJob( Job job) throws Exception {
		// TODO Auto-generated method stub
		Connection conn = null;
		String result = null;
		try {
			//to get connection to database
			conn = getConnection(conn);
			//to denied auto commit so we can rollback if program is error
			conn.setAutoCommit(false);
			result = dao.insertJob(conn, job);
			//to commit date edited using query above
			conn.commit();
		} catch (Exception e) {
			// TODO: handle exception if error happens
			conn.rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			conn.close();
		}
		return result;
	}

	@Override
	public String insertProposal( Proposal proposal) throws Exception {
		// TODO Auto-generated method stub
		Connection conn = null;
		String result = null;
		int checkJob = 0;
		try {
			//to get connection to database
			conn = getConnection(conn);
			//to denied auto commit so we can rollback if program is error
			conn.setAutoCommit(false);
			checkJob =dao.appliedCheck(conn, proposal.getFreelanceId());
			if(checkJob==0) {
				result = dao.insertProposal(conn, proposal);
				conn.commit();
			} else {
				result = "You Already Made Apply";
			}
		} catch (Exception e) {
			// TODO: handle exception
			conn.rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			conn.close();
		}
		return result;
	}

	@Override
	public String updateJob( Job job) throws Exception {
		// TODO Auto-generated method stub
		Connection conn = null;
		String result = null;
		try {
			//to get connection to database
			conn = getConnection(conn);
			//to denied auto commit so we can rollback if program is error
			conn.setAutoCommit(false);
			result = dao.updateJob(conn, job);
			conn.commit();
		} catch (Exception e) {
			// TODO: handle exception
			conn.rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			conn.close();
		}
		return result;
	}

	@Override
	public List<Job> allJobPublished() throws Exception {
		// TODO Auto-generated method stub
		Connection conn = null;
		List<Job> listJob = null;
		try {
			//to get connection to database
			conn = getConnection(conn);
			//to denied auto commit so we can rollback if program is error
			conn.setAutoCommit(false);
			listJob = dao.allJobPublished(conn);
		} catch (Exception e) {
			// TODO: handle exception
			conn.rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			conn.close();
		}
		return listJob;
	}

	@Override
	public List<Proposal> getProposal(Job job) throws Exception {
		// TODO Auto-generated method stub
		Connection conn = null;
		List<Proposal> listProposal = null;
		try {
			//to get connection to database
			conn = getConnection(conn);
			//to denied auto commit so we can rollback if program is error
			conn.setAutoCommit(false);
			listProposal = dao.getProposal(conn, job);
		} catch (Exception e) {
			// TODO: handle exception
			conn.rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			conn.close();
		}
		return listProposal;
	}

}
