package com.ajobthing.service;

import java.util.List;

import com.ajobthing.model.Job;
import com.ajobthing.model.Proposal;

public interface Service {
	public String insertJob(Job job) throws Exception;
	public String insertProposal(Proposal proposal)  throws Exception;
	public String updateJob(Job job)  throws Exception;
	public List<Job> allJobPublished()  throws Exception;
	public List<Proposal> getProposal(Job job) throws Exception;
}
