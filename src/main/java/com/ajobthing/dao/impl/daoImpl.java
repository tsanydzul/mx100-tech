package com.ajobthing.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Repository;

import com.ajobthing.constand.ConstandSQL;
import com.ajobthing.dao.Dao;
import com.ajobthing.model.Job;
import com.ajobthing.model.Proposal;

@Repository
public class daoImpl implements Dao{

	@Override
	public String insertJob(Connection conn, Job job) throws Exception {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String result = null;
		try {
			//set query
			String sql =  ConstandSQL.insertJob;
			ps = conn.prepareStatement(sql);
			//set question mark value on query
			ps.setString(1, job.getName());
			ps.setInt(2, job.getPublished());
			ps.executeUpdate();
			
			//check if data insert as draft or as publish (publish = 1 as publish, and publish = 0 as draft)
			if(job.getPublished()==0) {
				result = "Success Input Draft Job";
			}else {
				result = "Success Publish Job";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			ps.close();
		}
		return result;
	}

	@Override
	public String insertProposal(Connection conn, Proposal proposal) throws Exception {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String result = null;
		try {
			String sql =  ConstandSQL.insertProposal;
			ps = conn.prepareStatement(sql);
			ps.setInt(1, proposal.getJobId());
			ps.setInt(2, proposal.getFreelanceId());
			ps.setString(3, proposal.getProposal());
			ps.executeUpdate();
			result = "Success Input Proposal";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			ps.close();
		}
		return result;
	}

	@Override
	public String updateJob(Connection conn, Job job) throws Exception {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String result = null;
		try {
			String sql =  ConstandSQL.updateJob;
			ps = conn.prepareStatement(sql);
			ps.setInt(1, job.getPublished());
			ps.setInt(2, job.getId());
			ps.executeUpdate();
			
			//check if data insert as draft or as publish (publish = 1 as publish, and publish = 0 as draft)
			if(job.getPublished() == 0) {
				result = "Job Saved As Draft";
			} else {
				result = "Job Saved As Published";
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			ps.close();
		}
		return result;
	}

	@Override
	public List<Job> allJobPublished(Connection conn) throws Exception {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<Job> listJob = new ArrayList<Job>();
		try {
			Calendar now = Calendar.getInstance();  
		    //get current TimeZone
		    TimeZone timeZone = now.getTimeZone();
		    
			String sql = ConstandSQL.allJobPublished;
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				Job job = new Job();
				job.setId(rs.getInt("ID"));
				job.setName(rs.getString("NAME"));
				job.setPublished(rs.getInt("PUBLISHED"));
				
				//Get date data then convert it to local machine time zone
				job.setCreateddate(convertDate(rs.getString("CREATEDDATE"), timeZone));
				job.setUpdateddate(convertDate(rs.getString("UPDATEDDATE"), timeZone));
				
				listJob.add(job);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			ps.close();
			rs.close();
		}
		return listJob;
	}

	@Override
	public int appliedCheck(Connection conn, int freelanceId) throws Exception {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		PreparedStatement ps = null;
		int result = 0;
		try {
			String sql = ConstandSQL.appliedCheck;
			ps = conn.prepareStatement(sql);
			ps.setInt(1, freelanceId);
			rs = ps.executeQuery();
			while(rs.next()) {
				result = rs.getInt(1);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			ps.close();
			rs.close();
		}
		return result;
	}

	@Override
	public List<Proposal> getProposal(Connection conn, Job job) throws Exception {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<Proposal> result = new ArrayList<>();
		try {
			Calendar now = Calendar.getInstance();  
		    //get current TimeZone
		    TimeZone timeZone = now.getTimeZone();
			
			String sql = ConstandSQL.getProposal;
			ps = conn.prepareStatement(sql);
			ps.setInt(1, job.getId());
			rs = ps.executeQuery();
			while(rs.next()) {
				Proposal model = new Proposal();
				model.setJobId(rs.getInt("JOBID"));
				model.setFreelanceId(rs.getInt("FREELANCEID"));
				model.setProposal(rs.getString("PROPOSAL"));
				
				//Get date data then convert it to local machine time zone
				model.setApplieddate(convertDate(rs.getString("APPLIEDDATE"), timeZone));
				
				//insert all model to list model
				result.add(model);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			ps.close();
			rs.close();
		}
		return result;
	}
	
	public String convertDate(String date,TimeZone timezone) throws Exception {
		String result = null;
		try {
			if(date == null) {
				return result;	
			} else {
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
				Date dateCreated = formatter.parse(date);
				
				formatter.setTimeZone(timezone);
				result = formatter.format(dateCreated);
				return result;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

}
