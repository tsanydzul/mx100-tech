package com.ajobthing.dao;

import java.sql.Connection;
import java.util.List;

import com.ajobthing.model.Job;
import com.ajobthing.model.Proposal;

public interface Dao {
	public String insertJob(Connection conn, Job job)  throws Exception;
	public String insertProposal(Connection conn, Proposal proposal)  throws Exception;
	public String updateJob(Connection conn, Job job)  throws Exception;
	public List<Job> allJobPublished(Connection conn)  throws Exception;
	public List<Proposal> getProposal(Connection conn, Job job) throws Exception;
	public int appliedCheck(Connection conn, int freelanceId)  throws Exception;
}
