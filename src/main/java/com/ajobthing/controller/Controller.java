package com.ajobthing.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ajobthing.model.Job;
import com.ajobthing.model.Proposal;
import com.ajobthing.service.Service;

@RestController
@CrossOrigin()
public class Controller {
	
	@Autowired
	Service service;
	
	//Controller to insert job as draft or publish (depend on publish value)
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertJob", method = RequestMethod.POST)
	public <T>ResponseEntity<T> insertJob(@RequestBody Job job){
		ResponseEntity<T> entity = null;
		HashMap<String, String> hashMap = new HashMap<>();
		String hasil = null;
		try {
			hasil = service.insertJob(job);
			hashMap.put("message", hasil);
			entity = new ResponseEntity<T>((T) hashMap, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
			entity = new ResponseEntity<T>((T) e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	//Controller to insert proposal that freelance send
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertProposal", method = RequestMethod.POST)
	public <T>ResponseEntity<T> insertProposal(@RequestBody Proposal proposal){
		ResponseEntity<T> entity = null;
		HashMap<String, String> hashMap = new HashMap<>();
		String hasil = null;
		try {
			hasil = service.insertProposal(proposal);
			hashMap.put("message", hasil);
			entity = new ResponseEntity<T>((T) hashMap, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
			entity = new ResponseEntity<T>((T) e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	//Controller to change status of job either to draft or as publish
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/updateJob", method = RequestMethod.POST)
	public <T>ResponseEntity<T> updateJob(@RequestBody Job job){
		ResponseEntity<T> entity = null;
		HashMap<String, String> hashMap = new HashMap<>();
		String hasil = null;
		try {
			hasil = service.updateJob(job);
			hashMap.put("message", hasil);
			entity = new ResponseEntity<T>((T) hashMap, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
			entity = new ResponseEntity<T>((T) e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	//Controller for freelance to see all list posted job
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/allJobPublished", method = RequestMethod.GET)
	public <T>ResponseEntity<T> allJobPublished(){
		ResponseEntity<T> entity = null;
		List<Job> hasil = null;
		try {
			hasil = service.allJobPublished();
			entity = new ResponseEntity<T>((T) hasil, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
			entity = new ResponseEntity<T>((T) e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	//Controller to get all proposal freelance made on specific job id
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getProposal", method = RequestMethod.POST)
	public <T>ResponseEntity<T> getProposal(@RequestBody Job job){
		ResponseEntity<T> entity = null;
		List<Proposal> hasil = null;
		try {
			hasil = service.getProposal(job);
			entity = new ResponseEntity<T>((T) hasil, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			e.getMessage();
			entity = new ResponseEntity<T>((T) e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
}
