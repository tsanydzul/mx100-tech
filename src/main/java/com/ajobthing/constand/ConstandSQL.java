package com.ajobthing.constand;

public class ConstandSQL {
	public static String insertJob = "INSERT INTO JOB (NAME, PUBLISHED, CREATEDDATE) VALUES (?,?,UTC_TIMESTAMP)";
	public static String insertProposal = "INSERT INTO PROPOSAL (JOBID, FREELANCEID, PROPOSAL, APPLIEDDATE) VALUES (?,?,?,UTC_TIMESTAMP)";
	public static String updateJob = "UPDATE JOB SET PUBLISHED = ?, UPDATEDDATE=UTC_TIMESTAMP WHERE ID = ?"; 
	public static String allJobPublished = "SELECT * FROM JOB WHERE PUBLISHED = 1";
	public static String getProposal = "SELECT * FROM PROPOSAL WHERE JOBID = ?";
	public static String appliedCheck = "SELECT COUNT(*) FROM PROPOSAL WHERE FREELANCEID = ?";
}
