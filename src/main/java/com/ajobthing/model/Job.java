package com.ajobthing.model;

public class Job {
	private int id;
	private String name;
	private int published;
	private String createddate;
	private String updateddate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPublished() {
		return published;
	}
	public void setPublished(int published) {
		this.published = published;
	}
	public String getCreateddate() {
		return createddate;
	}
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}
	public String getUpdateddate() {
		return updateddate;
	}
	public void setUpdateddate(String updateddate) {
		this.updateddate = updateddate;
	}
	public Job(int id, String name, int published, String createddate, String updateddate) {
		super();
		this.id = id;
		this.name = name;
		this.published = published;
		this.createddate = createddate;
		this.updateddate = updateddate;
	}
	public Job() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
