package com.ajobthing.model;

public class Proposal {
	private int jobId;
	private int freelanceId;
	private String proposal;
	private String applieddate;
	
	public int getJobId() {
		return jobId;
	}
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
	public int getFreelanceId() {
		return freelanceId;
	}
	public void setFreelanceId(int freelanceId) {
		this.freelanceId = freelanceId;
	}
	public String getProposal() {
		return proposal;
	}
	public void setProposal(String proposal) {
		this.proposal = proposal;
	}
	public String getApplieddate() {
		return applieddate;
	}
	public void setApplieddate(String applieddate) {
		this.applieddate = applieddate;
	}
	public Proposal(int jobId, int freelanceId, String proposal, String applieddate) {
		super();
		this.jobId = jobId;
		this.freelanceId = freelanceId;
		this.proposal = proposal;
		this.applieddate = applieddate;
	}
	public Proposal() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
